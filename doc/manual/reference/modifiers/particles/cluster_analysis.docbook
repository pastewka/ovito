<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="particles.modifiers.cluster_analysis"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Cluster analysis</title>

  <para>
  
    <informalfigure>
      <informaltable frame="none" colsep="0" rowsep="0">
        <tgroup cols="2">
          <tbody>
            <row valign="bottom">
              <entry>Input:</entry>
              <entry>Output:</entry>
            </row>
            <row valign="top">
              <entry>
                <mediaobject><imageobject>
                <imagedata fileref="images/modifiers/cluster_analysis_example_input.png" format="PNG" scale="30" />
                </imageobject></mediaobject>
              </entry>
              <entry>
                <mediaobject><imageobject>
                <imagedata fileref="images/modifiers/cluster_analysis_example_output.png" format="PNG" scale="30" />
                </imageobject></mediaobject>
              </entry>
            </row>
          </tbody>
        </tgroup>
      </informaltable>
    </informalfigure>
      
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/modifiers/cluster_analysis_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>
  
    This modifier decomposes a particle system into disconnected groups of particles (clusters) based on a local neighboring criterion.
    The neighboring criterion can either be distance-based (i.e. a cutoff) or bond-based (graph topology).
  </para>
  
  <para>
    A cluster is defined as a set of connected particles, each of which is within the (indirect) reach of one or more 
    other particles from the same cluster. Thus, any two particles from the same cluster are connected by a 
    continuous path consisting of steps all fulfilling the selected neighboring criterion.
    Conversely, two particles will not belong to the same cluster if 
    there is no such continuous path on the neighbor network leading from one particle to the other.
  </para>
  <para>
    You can choose between the distance-based neighbor criterion, in which case two particles are considered
    neighbors if they are within a specified range of each other, and the bond-based criterion, in which case
    two particles are considered neighbors if they are connected by a bond. 
    A particle without any neighbors forms a single-particle cluster of its own.
  </para>
  <para>
    The modifier assigns numeric IDs to the clusters it forms (ranging from 1 to <emphasis>N</emphasis>, the total number of clusters).
    Each particle is assigned to one of these clusters and this information is output by the modifier as a new particle property named <literal>Cluster</literal>.
    Note that the numbering of clusters is arbitrary by default and depends on the order in which input particles are stored.
    You can activate the <emphasis>Sort clusters by size</emphasis> option to request an ordering of cluster IDs by number of contained particles.
    This guarantees that the first cluster (ID 1) will be the one with the largest number of particles.
  </para>

  <simplesect>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>Neighbor mode</term>
        <listitem>
          <para>Selects the criterion which is used to determine whether two particles are neighbors or not.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Cutoff distance</term>
        <listitem>
          <para>The range up to which two particles are considered neighbors.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Use only selected particles</term>
        <listitem>
          <para>
            This option restrict the clustering algorithm to currently selected
            particles. Unselected particles will be treated as if they do not exist and will be assigned
            the special cluster ID 0.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Sort clusters by size</term>
        <listitem>
          <para>
            This option sorts the clusters by size (in descending order). 
            Cluster ID 1 will be the largest cluster, cluster ID 2 the second largest, and so on.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
  
  <simplesect>
    <title>Exporting the modifier's results</title>
    <variablelist>
      <varlistentry>
        <term>Total number of clusters</term>
        <listitem>
          <para>To export the total number of clusters generated by the modifier to a text file (possibly as a function of time), 
          use OVITO's standard <link linkend="usage.export">file export function</link>. Choose "<emphasis>Table of values</emphasis>"
          as output format and make sure that the <code>ClusterAnalysis.cluster_count</code> global value, emitted by the modifier to the 
          data pipeline, gets exported. 
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Cluster particles</term>
        <listitem>
          <para>To export the list of particles belonging to each individual cluster, also use OVITO's
          standard <link linkend="usage.export">file export function</link>. Choose the <emphasis>XYZ</emphasis> output file format
          and select the <code>Cluster</code> property for export. This will produce a text file with the
          cluster ID of each particle.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Cluster sizes</term>
        <listitem>
          <para>Determining the size (i.e. the number of particles) of all clusters generated by the <emphasis>Cluster Analysis</emphasis> modifier 
          requires usage of the Python scripting interface of OVITO. You can copy/paste the following Python script to a <literal>.py</literal> file and execute it using 
          the <menuchoice><guimenu>File</guimenu><guimenuitem>Run Script File</guimenuitem></menuchoice> function:</para>
    <para><programlisting>import ovito
import numpy

output_filepath = "cluster_sizes.txt"
data = ovito.scene.selected_pipeline.compute()
cluster_sizes = numpy.bincount(data.particles['Cluster'])
numpy.savetxt(output_filepath, cluster_sizes)
    </programlisting></para>
    <para>
      Your should adjust the output file path in the script as needed. The script makes use
      of the <link xlink:href="http://docs.scipy.org/doc/numpy/reference/generated/numpy.bincount.html#numpy.bincount"><code>bincount()</code></link> Numpy function to count the 
      number of particles belonging to each cluster. Note that the array returned by this function includes cluster ID 0, which is normally not assigned by the modifier
      and therefore typically has size zero. For more information on OVITO's scripting interface, see <link linkend="usage.scripting">this section</link>.
    </para>
    <para>
      It is possible to perform the file export for every frame in a simulation sequence by adding a <code>for</code>-loop to the script:</para>
<para><programlisting>import ovito
import numpy

for frame in range(ovito.scene.selected_pipeline.source.num_frames):
    output_filepath = "cluster_sizes.%i.txt" % frame
    data = ovito.scene.selected_pipeline.compute(frame)
    cluster_sizes = numpy.bincount(data.particles['Cluster'])
    numpy.savetxt(output_filepath, cluster_sizes)
    </programlisting></para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
  
  <simplesect>
  <title>See also</title>
    <para>
      <link xlink:href="python/modules/ovito_modifiers.html#ovito.modifiers.ClusterAnalysisModifier"><classname>ClusterAnalysisModifier</classname> (Python API)</link>
    </para>
  </simplesect>  
  
</section>
