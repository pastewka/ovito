from ovito.io import import_file
from ovito.vis import VectorVis
from ovito.modifiers import CalculateDisplacementsModifier

# >>>>>>>>
pipeline = import_file('input/simulation.dump')
pipeline.add_to_scene()
vector_vis = pipeline.get_vis(VectorVis)
vector_vis.color = (1,0,0)
# <<<<<<<<


# >>>>>>>>
modifier = CalculateDisplacementsModifier()
pipeline.modifiers.append(modifier)
modifier.vis.enabled = True
modifier.vis.shading = VectorVis.Shading.Flat
# <<<<<<<<
